﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManagerTimber : MonoBehaviour {
    [SerializeField]GameObject[] TroncosArbol;
    [SerializeField] GameObject myPlayer;
    [SerializeField] GameObject Stand;
    [SerializeField] GameObject ButtonStart;
    [SerializeField] TextMeshProUGUI Score;
    Animator standController;
    public int ArrayCounter = 0;
    public static bool fighting = false;
    public static bool fightingYet = false;
    public static bool CorroutineON = false;
    public bool GameStart = false;
    public int ScoreNmb = 0;


    public void Start()
    {
        standController = Stand.GetComponent<Animator>();   
    }
    public void Update()
    {
        standController.SetBool("fight", fighting);
    }
    // Use this for initialization
    public void OnClickLeft()
    {
        if (GameStart)
        {
            myPlayer.GetComponent<RectTransform>().localScale = new Vector3(1.3F, 1.3F, 1);
            myPlayer.GetComponent<RectTransform>().localPosition = new Vector3(-118, -300, 0);
            fighting = true;
            fightingYet = true;
            ArbolVa();
        }
        
    }
    public void OnClickRight()
    {
        if (GameStart)
        {
            myPlayer.GetComponent<RectTransform>().localScale = new Vector3(-1.3F, 1.3F, 1);
            myPlayer.GetComponent<RectTransform>().localPosition = new Vector3(118, -300, 0);
            fighting = true;
            fightingYet = true;
            ArbolVa();
        }
    }
    private void ArbolVa()
    {
        if (!CorroutineON) {
            StartCoroutine(stopFight());
            CorroutineON = true;
        }
        
        fightingYet = false;
        TroncosArbol[ArrayCounter].GetComponent<RectTransform>().localPosition = new Vector3(0, 660, 0);
        ScoreNmb++;
        Score.SetText(ScoreNmb.ToString());
        GameObject tmp = TroncosArbol[ArrayCounter];
        
        int R = Random.Range(0, 3);
        Debug.Log(R);
        if (R == 0)
        {
            tmp.transform.GetChild(0).GetComponent<RectTransform>().localPosition=new Vector3(-96, tmp.transform.GetChild(0).transform.localPosition.y, tmp.transform.GetChild(0).transform.localPosition.z);
            tmp.transform.GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(396, tmp.transform.GetChild(1).transform.localPosition.y, tmp.transform.GetChild(1).transform.localPosition.z);
            
        }else if (R == 1){
            tmp.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(-396, tmp.transform.GetChild(0).transform.localPosition.y, tmp.transform.GetChild(0).transform.localPosition.z);
            tmp.transform.GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(96, tmp.transform.GetChild(1).transform.localPosition.y, tmp.transform.GetChild(1).transform.localPosition.z);
        }
        else if(R==2)
        {
            tmp.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(-396, tmp.transform.GetChild(0).transform.localPosition.y, tmp.transform.GetChild(0).transform.localPosition.z);
            tmp.transform.GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(396, tmp.transform.GetChild(1).transform.localPosition.y, tmp.transform.GetChild(1).transform.localPosition.z);
        }
        ArrayCounter++;
        if (ArrayCounter >= 9)
            ArrayCounter = 0;
        for (int i = 0; i < 9; i++)
        {
            TroncosArbol[i].GetComponent<RectTransform>().Translate(new Vector3(0,-1.04F,0)); 
        }
      
    }
    public void StartGame()
    {
        GameStart = true;
        ButtonStart.SetActive(false);
        ScoreNmb = 0;
        Score.SetText("0");
    }
    public void EndGame()
    {
        ButtonStart.SetActive(true);
        GameStart = false;
        fighting = false;

        for (int i = 0; i < 9; i++)
        {
            GameObject tmp = TroncosArbol[i];

            tmp.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(-396, tmp.transform.GetChild(0).transform.localPosition.y, tmp.transform.GetChild(0).transform.localPosition.z);
            tmp.transform.GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(396, tmp.transform.GetChild(1).transform.localPosition.y, tmp.transform.GetChild(1).transform.localPosition.z);

        }
    }
    public IEnumerator stopFight()
    {
        yield return new WaitForSeconds(1.5F);
        if (!fightingYet)
        {
            fighting = false;
        }
        CorroutineON = false;
        
    }
   
}
