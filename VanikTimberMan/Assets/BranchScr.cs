﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BranchScr : MonoBehaviour {
    [SerializeField] GameObject GameManager;
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.GetComponent<GameManagerTimber>().EndGame();
        }
    }
}
