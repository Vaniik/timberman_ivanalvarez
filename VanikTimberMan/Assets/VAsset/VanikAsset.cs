﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VAsset{
    public static class VKCamEffects {

        private static Camera camera;
        private static Transform cameraTransform;
        public static Vector3 startPosition;

        //--ShakeVars

        public static float slowDownAmount = 1.0F;
        public static float power = 0.1F;
        public static float duration = 3.0F;

        //--ZoomVars
        public static float zoomPower;
        public static float normal;
        public static float smooth;
        public static float iniFOV;

        public static void Start()
        {
            camera = Camera.main;
            cameraTransform=Camera.main.transform;// cojo la camara principal
            startPosition=cameraTransform.localPosition;
            normal = camera.fieldOfView;
            
        }


        //---------------SHAKES----------------

            
            /// Hacer funcion ProShakes que coja un vector (1,1,1)

        public static class Shakes
        {
            //----------------> Horizontal Shake
            public static IEnumerator XShake(float d)
            {

                duration = d;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("X");

                    if (duration > 0.2)
                    {

                        Vector3 tempPos = new Vector3(Random.Range(-1.0F, 1.0F), 0, 0);
                        cameraTransform.localPosition = startPosition + tempPos * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }
            public static IEnumerator XShake(float d, float sd)
            {

                duration = d;
                slowDownAmount = sd;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("X");

                    if (duration > 0.2)
                    {

                        Vector3 tempPos = new Vector3(Random.Range(-1.0F, 1.0F), 0, 0);
                        cameraTransform.localPosition = startPosition + tempPos * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }

            //----------------------------->Vertical Shakes

            public static IEnumerator YShake(float d)
            {

                duration = d;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("Y");

                    if (duration > 0.2)
                    {

                        Vector3 tempPos = new Vector3(0, Random.Range(-1.0F, 1.0F), 0);
                        cameraTransform.localPosition = startPosition + tempPos * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }

            public static IEnumerator YShake(float d, float sd)
            {

                duration = d;
                slowDownAmount = sd;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("Y");

                    if (duration > 0.2)
                    {

                        Vector3 tempPos = new Vector3(0, Random.Range(-1.0F, 1.0F), 0);
                        cameraTransform.localPosition = startPosition + tempPos * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }

            //----------------------------->Depth Shakes (As dessigner i recommend don't to use)
            public static IEnumerator ZShake(float d)
            {

                duration = d;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("Z");

                    if (duration > 0.2)
                    {

                        Vector3 tempPos = new Vector3(0, 0, Random.Range(-1.0F, 1.0F));
                        cameraTransform.localPosition = startPosition + tempPos * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }

            public static IEnumerator ZShake(float d, float sd)
            {

                duration = d;
                slowDownAmount = sd;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("Z");

                    if (duration > 0.2)
                    {

                        Vector3 tempPos = new Vector3(0, 0, Random.Range(-1.0F, 1.0F));
                        cameraTransform.localPosition = startPosition + tempPos * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }

            //----------------------------->Sphere Shakes;
            public static IEnumerator SphereShake(float d)
            {

                duration = d;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("Sphere");

                    if (duration > 0.2)
                    {
                        cameraTransform.localPosition = startPosition + Random.insideUnitSphere * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }


            public static IEnumerator SphereShake(float d, float sd)
            {

                duration = d;
                slowDownAmount = sd;
                for (float f = duration; f >= 0; f -= Time.deltaTime)
                {
                    Debug.Log("Sphere");

                    if (duration > 0.2)
                    {
                        cameraTransform.localPosition = startPosition + Random.insideUnitSphere * power * duration;
                        duration -= Time.deltaTime * slowDownAmount;
                    }
                    else
                    {
                        f = 0;
                        cameraTransform.localPosition = startPosition;
                    }

                    yield return null;
                }

            }

        }

        //-------------------FIN SHAKES-----------------
        //---------------------ZOOMS--------------------
        public static class Zooms
        {

            public static IEnumerator ZoomIn( float d)
            {
                iniFOV = camera.fieldOfView;
                for (float f = 1; f >= 0; f -= Time.deltaTime)
                {
                   //Debug.Log(d-f);
                    if (f > 0.5)
                    {
                        Debug.Log("Entro");
                        camera.fieldOfView = iniFOV * f;
                    }
                    else if (f > 0.4 && f < 0.6)
                    {
                        Debug.Log("Stop, wait a minute");
                        yield return new WaitForSeconds(d/6);
                    }else
                    {
                        Debug.Log("salgo");
                        camera.fieldOfView = iniFOV * (1 - f);

                        
                    }
                    yield return null;
                }


            }

        }
        //-------------------FIN ZOOMS------------------

        // -----------------Camara Lenta----------------

        public static class Tempo
        {
            public static IEnumerator SlowMotion(float d)
            {
                for (float f = 1; f > 0; f -= Time.deltaTime)
                {
                   
                    if (f >= 0.5f)
                    {
                        Debug.Log("SlowMotion");
                        Time.timeScale=f;
                    }
                    else if (f > 0.4 && f < 0.6)
                    {
                        Time.timeScale = 0.5f;
                        Debug.Log("Stop, wait a minute");
                        yield return new WaitForSeconds(d/6);
                    }
                    else
                    {
                        Debug.Log("fastMotion");
                        Time.timeScale = 1-f;
                    }
                    yield return null;
                }

            }

            


        }

        //----------------Fin Camara Lenta--------------


    }
}
